import cvxpy as cp
import numpy as np
from partial_trace import partial_trace
import math
from numpy.core.numeric import outer

class alice_sdp:

    # Constructor
    def __init__(self, states):
        self.states = states
        self.D = self.states.shape[1]

    # Creating variables for Alice's primal SDP
    def __create_sdp_var(self):
        len_state = self.states.shape[0]
        list_sigma = [cp.Variable((len_state, len_state), PSD = True) for x in range(self.D)]
        sqrt_len_state = int(math.sqrt(len_state))
        sig = cp.Variable((sqrt_len_state, sqrt_len_state), PSD = True)

        return list_sigma, sig

    # Adding constraints and objective to Alice's SDP
    def __add_constraints(self, list_sigma, sig):
        constr = []
        for i in range(self.D):
            qit_instance = partial_trace(list_sigma[i])
            constr += [qit_instance.trace_out_first() == sig]
        constr += [cp.trace(sig) == 1]

        obj = 0
        for i in range(self.D):            
            obj += cp.trace(list_sigma[i] @ outer(self.states[:,i], self.states[:,i]))

        return obj / self.D, constr

    # Solving the SDP
    def solve_opt(self):

        list_sigma, sig = self.__create_sdp_var()
        obj, constr = self.__add_constraints(list_sigma, sig)
        prob = cp.Problem(cp.Maximize(obj), constr)
        result = prob.solve()

        print("Alice's: ", prob.status, obj.value)