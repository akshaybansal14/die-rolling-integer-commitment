import cvxpy as cp
import numpy as np
from numpy.core.numeric import outer
from partial_trace import partial_trace
import math


class bob_sdp:

    # Constructor
    def __init__(self, states):
        self.states = states
        self.D = self.states.shape[1]

    # Creating variables for Bob's primal SDP
    def __create_sdp_var(self):
        len_state = self.states.shape[0]
        sqrt_len_state = int(math.sqrt(len_state))
        list_M = [cp.Variable((sqrt_len_state, sqrt_len_state), PSD = True) for x in range(self.D)]
        
        return list_M

    # Adding constraints and objective to Bob's SDP
    def __add_constraints(self, list_M):
        constr = [sum(list_M) == np.identity(list_M[0].shape[0])]
        obj = 0
        for i in range(self.D):
            qit_instance = partial_trace(outer(self.states[:,i], self.states[:,i]))
            obj += cp.trace(list_M[i] @ qit_instance.trace_out_first())

        return obj / self.D, constr

    # Solving the SDP
    def solve_opt(self):

        list_M = self.__create_sdp_var()
        obj, constr = self.__add_constraints(list_M)
        prob = cp.Problem(cp.Maximize(obj), constr)
        result = prob.solve()

        print("Bob's: ", prob.status, obj.value)