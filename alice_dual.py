import cvxpy as cp
import numpy as np
from partial_trace import partial_trace
import math
from numpy.core.numeric import identity, outer
from toqito.matrix_ops import tensor

class alice_dual:

    # Constructor
    def __init__(self, states):
        self.states = states
        self.D = self.states.shape[1]

    # Creating variables for Alice's primal SDP
    def __create_sdp_var(self):
        len_state = self.states.shape[0]
        sqrt_len_state = int(math.sqrt(len_state))
        s = cp.Variable()
        list_Z = [cp.Variable((sqrt_len_state, sqrt_len_state)) for x in range(self.D)]
        list_psi = [cp.Variable((len_state, len_state), PSD = True) for x in range(self.D)]

        return s, list_Z, list_psi

    # Adding constraints and objective to Alice's SDP
    def __add_constraints(self, s, list_Z, list_psi):
        len_state = self.states.shape[0]
        sqrt_len_state = int(math.sqrt(len_state))
        constr = []
        constr += [s * identity(sqrt_len_state) >> sum(list_Z)]
        for i in range(self.D):
            # constr += [cp.bmat(tensor(identity(sqrt_len_state), list_Z[i])) >> (1 / self.D) * outer(self.states[:,i], self.states[:,i]), list_Z[i] == list_Z[i].T]
            constr += [cp.bmat(tensor(identity(sqrt_len_state), list_Z[i])) >> (1 / self.D) * list_psi[i], list_Z[i] == list_Z[i].T, cp.trace(list_psi[i]) == 1]
        obj = s
        return obj, constr

    # Solving the SDP
    def solve_opt(self):

        s, list_Z, list_psi = self.__create_sdp_var()
        obj, constr = self.__add_constraints(s, list_Z, list_psi)
        prob = cp.Problem(cp.Minimize(obj), constr)
        result = prob.solve()

        print("Alice's dual: ", prob.status, obj.value)