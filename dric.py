from math import sqrt
from alice_sdp import alice_sdp
from bob_sdp import bob_sdp
from alice_dual import alice_dual
from bob_dual import bob_dual
from alice_bob_dual import alice_bob_dual
import numpy as np
import qutip as qp

def main():
    psi_0 = np.asarray(qp.tensor(qp.basis(2,0), qp.basis(2,0))).real
    psi_1 = (1 / sqrt(2)) * np.asarray(qp.tensor(qp.basis(2,0), qp.basis(2,0)) + qp.tensor(qp.basis(2,1), qp.basis(2,1))).real

    # dim  = 4
    # psi_0 = (1 / np.sqrt(3)) * np.asarray(qp.tensor(qp.basis(dim,0), qp.basis(dim,0)) + qp.tensor(qp.basis(dim,1), qp.basis(dim,1)) + qp.tensor(qp.basis(dim,2), qp.basis(dim,2))).real
    # psi_1 = (1 / np.sqrt(3)) * np.asarray(qp.tensor(qp.basis(dim,0), qp.basis(dim,0)) + qp.tensor(qp.basis(dim,2), qp.basis(dim,2)) + qp.tensor(qp.basis(dim,3), qp.basis(dim,3))).real
    # psi_2 = (1 / np.sqrt(3)) * np.asarray(qp.tensor(qp.basis(dim,0), qp.basis(dim,0)) + qp.tensor(qp.basis(dim,1), qp.basis(dim,1)) + qp.tensor(qp.basis(dim,3), qp.basis(dim,3))).real
    
    states = np.hstack([psi_0, psi_1])

    alice_instance = alice_sdp(states)
    bob_instance = bob_sdp(states)
    alice_dual_instance = alice_dual(states)
    bob_dual_instance = bob_dual(states)
    alice_instance.solve_opt()
    bob_instance.solve_opt()
    alice_dual_instance.solve_opt()
    bob_dual_instance.solve_opt()

    alice_bob_dual_instance = alice_bob_dual(states)
    alice_bob_dual_instance.solve_opt()

    return

if __name__ == '__main__':
    main()