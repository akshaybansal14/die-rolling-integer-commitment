import qutip as qp
import numpy as np
import math
import sys

# Class to implement partial trace for a system of two qubits with same register size
class partial_trace:

    def __init__(self, rho):
        self.rho = rho
        if (not math.isqrt(self.rho.shape[0])):
            print("Error: Dimension of the two qubits are different")
            sys.exit()
        self.len_basis = int(math.sqrt(self.rho.shape[0]))

    # Method to trace out the first qubit and return a cvx type variable
    def trace_out_first(self):
        partial_trace = 0
        for i in range(self.len_basis):
            partial_trace += np.asarray(qp.tensor(qp.basis(self.len_basis, i), qp.identity(self.len_basis)).dag()).real @ self.rho @ np.asarray(qp.tensor(qp.basis(self.len_basis, i), qp.identity(self.len_basis))).real

        return partial_trace

    # Method to trace out the second qubit and return a cvx type variable
    def trace_out_second(self):
        partial_trace = 0
        for i in range(self.len_basis):
            partial_trace += np.asarray(qp.tensor(qp.identity(self.len_basis), qp.basis(self.len_basis, i)).dag()).real @ self.rho @ np.asarray(qp.tensor(qp.identity(self.len_basis), qp.basis(self.len_basis, i))).real

        return partial_trace