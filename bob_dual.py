import cvxpy as cp
import numpy as np
from partial_trace import partial_trace
import math
from numpy.core.numeric import identity, outer
from toqito.matrix_ops import tensor

class bob_dual:

    # Constructor
    def __init__(self, states):
        self.states = states
        self.D = self.states.shape[1]

    # Creating variables for Alice's primal SDP
    def __create_sdp_var(self):
        len_state = self.states.shape[0]
        sqrt_len_state = int(math.sqrt(len_state))
        X = cp.Variable((sqrt_len_state, sqrt_len_state))
        list_psi = [cp.Variable((len_state, len_state), PSD = True) for x in range(self.D)]
        
        return X, list_psi

    # Adding constraints and objective to Alice's SDP
    def __add_constraints(self, X, list_psi):
        constr = []
        for i in range(self.D):
            # qit_instance = partial_trace(outer(self.states[:,i], self.states[:,i]))
            qit_instance = partial_trace(list_psi[i])
            constr += [X >> (1 / self.D) * qit_instance.trace_out_first(), cp.trace(list_psi[i]) == 1]
        obj = cp.trace(X)
        return obj, constr

    # Solving the SDP
    def solve_opt(self):

        X, list_psi = self.__create_sdp_var()
        obj, constr = self.__add_constraints(X, list_psi)
        prob = cp.Problem(cp.Minimize(obj), constr)
        result = prob.solve()

        print("Bob's dual: ", prob.status, obj.value)